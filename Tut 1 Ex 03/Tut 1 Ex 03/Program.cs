﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tut_1_Ex_03
{
    class Program
    {
        static void Main(string[] args)
        {
            var myName = string.Empty;
            Console.WriteLine("Hello, please enter your name!");
            myName = Console.ReadLine();
            Console.WriteLine($"Thank you {myName}!");
            
        }
    }
}
